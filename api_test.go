package qs

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestSession(t *testing.T) {
	tk := Token{
		Access:  "thisistheaccesstoken",
		Refresh: "thisistherefreshtoken",
	}
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if url := req.URL.String(); url != "/sessions" {
			t.Fatalf("Invalid URL: %q", url)
		}
		json.NewEncoder(rw).Encode(tk)
	}))

	cln, err := NewClient(server.URL)
	if err != nil {
		t.Fatal(err)
	}

	err = cln.Session(&url.Userinfo{})
	if err != nil {
		t.Fatal(err)
	}

	if !cln.ValidSession() {
		t.Error("Session log-in failed")
	}
}

func TestRefresh(t *testing.T) {
	tk := Token{
		Access:  "thisistheaccesstoken",
		Refresh: "thisistherefreshtoken",
		expires: time.Now().Add(time.Minute * 5),
	}

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if url := req.URL.String(); url != "/session/refresh" {
			t.Fatalf("Invalid URL: %q", url)
		}
		if req.Method != "POST" {
			t.Fatalf("Invalid method: %q", req.Method)
		}
		defer req.Body.Close()

		var val string
		json.NewDecoder(req.Body).Decode(&val)
		if val != tk.Refresh {
			t.Fatalf("Invalid refresh token: %q", val)
		}
		tk2 := tk
		tk2.Access += "-new"
		tk2.Refresh += "-new"
		json.NewEncoder(rw).Encode(tk2)
	}))

	cln, err := NewClient(server.URL)
	if err != nil {
		t.Fatal(err)
	}
	cln.token = tk

	if err := cln.Refresh(); err != nil {
		t.Fatal(err)
	}

	if !strings.HasSuffix(cln.token.Access, "-new") {
		t.Errorf("Access token not updated: %#v", cln.token)
	}

	if !cln.token.expires.After(tk.expires) {
		t.Errorf("Expiration time not updated: %#v", cln.token.expires)
	}

}

func TestIrdStatus(t *testing.T) {
	tk := Token{
		Access:  "thisistheaccesstoken",
		Refresh: "thisistherefreshtoken",
		expires: time.Now().Add(time.Minute * 5),
	}

	ic := IrdConstellation{
		Visible:  true,
		SigLevel: -111,
		SigBars:  3,
	}

	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if url := req.URL.String(); url != "/device/ird/constellation" {
			t.Fatalf("Invalid URL: %q", url)
		}
		if req.Method != "GET" {
			t.Fatalf("Invalid method: %q", req.Method)
		}

		defer req.Body.Close()

		json.NewEncoder(rw).Encode(ic)
	}))

	cln, err := NewClient(server.URL)
	if err != nil {
		t.Fatal(err)
	}
	cln.token = tk

	result, err := cln.IridiumStatus()
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(ic, result) {
		t.Errorf("Bad result; expected %#v, got %#v", ic, result)
	}

}

func TestFwRules(t *testing.T) {
	out := `[{
                "name": "icmp",
                "protocol":     "icmp",
                "destAddr":     "any",
                "destAddrNeg":  false,
                "destPort":     "any",
                "destPortNeg":  false,
                "action":       "accept"
        }]`
	buf := bytes.NewBufferString(out)

	rules := make([]FwRule, 0)
	if err := json.NewDecoder(buf).Decode(&rules); err != nil {
		t.Fatal(err)
	}
}

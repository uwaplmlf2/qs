// Package qs implements an HTTP client for the Quicksilver Iridium Modem web API
package qs

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

var tokenValid time.Duration = time.Second * 870

// Map HTTP status codes to errors
var errorTable map[int]error = map[int]error{
	400: errors.New("invalid request"),
	401: errors.New("not authorized"),
	403: errors.New("operation forbidden"),
	405: errors.New("method not allowed"),
}

var (
	ErrInternal     = errors.New("internal server error")
	ErrInvalidToken = errors.New("invalid token")
)

func httpError(code int) error {
	if code >= 500 {
		return ErrInternal
	}

	return errorTable[code]
}

// Token contains the API authentication token
type Token struct {
	Access  string    `json:"token"`
	Refresh string    `json:"refreshToken"`
	expires time.Time `json:"-"`
}

func (t Token) IsValid() bool {
	return t.Access != "" && t.Refresh != "" && t.expires.After(time.Now().UTC())
}

func (t Token) String() string {
	return "Bearer " + t.Access
}

// Store writes the external representation of a Token to an io.Writer
func (t Token) Store(w io.Writer) error {
	m := make(map[string]interface{})
	m["token"] = t.Access
	m["refreshToken"] = t.Refresh
	m["expires"] = t.expires
	return json.NewEncoder(w).Encode(m)
}

// Load reads the external representation of a Token from an io.Reader
func (t *Token) Load(r io.Reader) error {
	m := make(map[string]interface{})
	if err := json.NewDecoder(r).Decode(&m); err != nil {
		return err
	}

	// Validate
	for _, key := range []string{"token", "refreshToken", "expires"} {
		val, ok := m[key]
		if !ok {
			return fmt.Errorf("Missing attribute: %q", key)
		}
		if val == nil {
			return fmt.Errorf("Empty attribute: %q", key)
		}
		_, ok = m[key].(string)
		if !ok {
			return fmt.Errorf("Invalid type for %q; %T", key, m[key])
		}
	}

	t.Access = m["token"].(string)
	t.Refresh = m["refreshToken"].(string)

	return t.expires.UnmarshalText([]byte(m["expires"].(string)))
}

type Client struct {
	*http.Client
	u     *url.URL
	token Token
}

// NewClient returns a new web API client, apiUrl is the root URL.
func NewClient(apiUrl string) (*Client, error) {
	u, err := url.Parse(apiUrl)
	if err != nil {
		return nil, err
	}
	// Work-around for the modem's self-signed SSL certificate
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	return &Client{
		Client: &http.Client{Transport: tr},
		u:      u,
	}, nil
}

// Session starts a new user session. The session will expire in approximately 15
// minutes but can be renewed using the Refresh method.
func (c *Client) Session(creds *url.Userinfo) error {
	u := *c.u
	u.User = creds
	u.Path = u.Path + "/sessions"
	req, err := http.NewRequest(http.MethodPost, u.String(), nil)
	if err != nil {
		return err
	}
	req.Header.Add("Accept", "application/json")

	t0 := time.Now().UTC()
	resp, err := c.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if err = httpError(resp.StatusCode); err != nil {
		return err
	}

	if err := json.NewDecoder(resp.Body).Decode(&c.token); err != nil {
		return err
	}
	c.token.expires = t0.Add(tokenValid)

	return nil
}

func (c *Client) CloseSession() error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/session"

	resp, err := c.makeRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return err
	}
	// Invalidate the session tokens
	c.token = Token{}

	return nil
}

func (c *Client) ValidSession() bool {
	return c.token.IsValid()
}

func (c *Client) makeRequest(method, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", c.token.String())
	return c.Do(req)
}

// Refresh renews the login session tokens
func (c *Client) Refresh() error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	enc.Encode(c.token.Refresh)
	u := *c.u
	u.Path = u.Path + "/session/refresh"

	t0 := time.Now()
	resp, err := c.makeRequest(http.MethodPost, u.String(), &b)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return err
	}

	if err := json.NewDecoder(resp.Body).Decode(&c.token); err != nil {
		return err
	}
	c.token.expires = t0.Add(tokenValid)

	return nil
}

func (c *Client) irdGet(which string, v interface{}) error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/device/ird/" + which

	resp, err := c.makeRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return fmt.Errorf("makeRequest: %w", err)
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return err
	}

	return json.NewDecoder(resp.Body).Decode(v)
}

// IridiumStatus returns the status of the connection to the Iridium satellite constellation.
func (c *Client) IridiumStatus() (IrdConstellation, error) {
	var ic IrdConstellation
	err := c.irdGet("constellation", &ic)
	return ic, err
}

// IridiumInfo returns the Iridium modem hardware information.
func (c *Client) IridiumInfo() (IrdHw, error) {
	var hw IrdHw
	err := c.irdGet("hw", &hw)
	return hw, err
}

// IridiumSim returns the Iridium SIM card information
func (c *Client) IridiumSim() (IrdSim, error) {
	var sim IrdSim
	err := c.irdGet("sim", &sim)
	return sim, err
}

// WifiEnable is used to enable or disable the Wifi interface for both the
// runtime and startup configurations.
func (c *Client) WifiEnable(state bool) error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	data := make(map[string]interface{})
	var b bytes.Buffer

	for _, cfgtype := range []string{"runtime", "startup"} {
		u := *c.u
		u.Path = u.Path + "/cfg/" + cfgtype + "/wifi"
		resp, err := c.makeRequest(http.MethodGet, u.String(), nil)
		if err != nil {
			return fmt.Errorf("GET %s: %w", cfgtype, err)
		}

		if err = httpError(resp.StatusCode); err != nil {
			resp.Body.Close()
			return fmt.Errorf("GET %s: %w", cfgtype, err)
		}
		err = json.NewDecoder(resp.Body).Decode(&data)
		resp.Body.Close()
		if err != nil {
			return fmt.Errorf("GET %s: %w", cfgtype, err)
		}
		b.Reset()
		data["enabled"] = state
		json.NewEncoder(&b).Encode(data)

		resp, err = c.makeRequest(http.MethodPatch, u.String(), &b)
		if err != nil {
			return fmt.Errorf("PATCH %s: %w", cfgtype, err)
		}
		resp.Body.Close()

		if err = httpError(resp.StatusCode); err != nil {
			return fmt.Errorf("PATCH %s: %w", cfgtype, err)
		}
	}

	return nil
}

// AddFwRule appends a new rule to the modem's outgoing firewall
func (c *Client) AddFwRule(cfgtype CfgType, rule FwRule) error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/cfg/" + string(cfgtype) + "/outgoingFirewall/rules"
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	enc.Encode(rule)

	resp, err := c.makeRequest(http.MethodPost, u.String(), &b)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return httpError(resp.StatusCode)
}

// DelFwRule deletes a firewall rule
func (c *Client) DelFwRule(cfgtype CfgType, name string) error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/cfg/" + string(cfgtype) + "/outgoingFirewall/rules/" + name

	resp, err := c.makeRequest(http.MethodDelete, u.String(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return httpError(resp.StatusCode)
}

// FwRules returns the current list of outgoing firewall rules
func (c *Client) FwRules(cfgtype CfgType) ([]FwRule, error) {
	if !c.token.IsValid() {
		return nil, ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/cfg/" + string(cfgtype) + "/outgoingFirewall/rules"
	resp, err := c.makeRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return nil, err
	}

	rules := make([]FwRule, 0)
	err = json.NewDecoder(resp.Body).Decode(&rules)

	return rules, err
}

// Reboot restarts the modem. The session will be closed. Wait at least
// 15 seconds before trying to reconnect.
func (c *Client) Reboot() error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	u := *c.u
	u.Path = u.Path + "/device/reboot"
	resp, err := c.makeRequest(http.MethodPost, u.String(), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return httpError(resp.StatusCode)
}

// IridiumActivate sets the mode of the Iridium interface, inactive (false) or
// data (true). The mode is only set for the runtime configuration.
func (c *Client) IridiumActivate(enable bool) error {
	if !c.token.IsValid() {
		return ErrInvalidToken
	}

	var mode string
	if enable {
		mode = "data"
	} else {
		mode = "inactive"
	}

	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	enc.Encode(mode)

	u := *c.u
	u.Path = u.Path + "/cfg/runtime/ird/mode"
	resp, err := c.makeRequest(http.MethodPut, u.String(), &b)
	if err != nil {
		return err
	}
	resp.Body.Close()

	return httpError(resp.StatusCode)
}

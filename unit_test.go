package qs

import (
	"bytes"
	"errors"
	"reflect"
	"testing"
	"time"
)

func testParse(t *testing.T, in string, expect FwRule, e error) {
	rule := FwRule{}
	err := rule.ParseText([]byte(in))
	if !errors.Is(err, e) {
		t.Fatalf("Unexpected error: %v", err)
	}

	if err != nil {
		return
	}

	if !reflect.DeepEqual(rule, expect) {
		t.Errorf("Parse failed; expected %#v, got %#v", expect, rule)
	}
}

func testFmt(t *testing.T, in FwRule, expect string) {
	b, err := in.FmtText()
	if err != nil {
		t.Fatal(err)
	}
	if text := string(b); text != expect {
		t.Errorf("Bad output; expected %q, got %q", expect, text)
	}
}

func TestFwRuleParse(t *testing.T) {
	tests := map[string]struct {
		in  string
		err error
		out FwRule
	}{
		"single": {in: "test allow tcp 128.95.97.213/32 10130",
			out: FwRule{Name: "test", DestAddr: "128.95.97.213/32", DestPort: "10130",
				Protocol: "tcp", Action: "accept"}},
		"multi": {in: "test deny tcp 128.95.97.213/32 8080-8090",
			out: FwRule{Name: "test", DestAddr: "128.95.97.213/32", DestPort: "8080-8090",
				Protocol: "tcp", Action: "drop"}},
		"bad-proto":  {in: "test allow foo 128.95.97.213/32 22", err: ErrProto},
		"bad-action": {in: "test want tcp 128.95.97.213/32 22", err: ErrAction},
		"negate-addr": {in: "test allow tcp !128.95.97.0/24 80",
			out: FwRule{Name: "test", DestAddr: "128.95.97.0/24", DestAddrNeg: true,
				DestPort: "80", Protocol: "tcp", Action: "accept"}},
		"negate-port": {in: "test allow tcp 128.95.97.0/24 !80",
			out: FwRule{Name: "test", DestAddr: "128.95.97.0/24", DestPortNeg: true,
				DestPort: "80", Protocol: "tcp", Action: "accept"}},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testParse(t, tc.in, tc.out, tc.err)
		})
	}
}

func TestFwRuleFmt(t *testing.T) {
	tests := map[string]struct {
		in  FwRule
		out string
	}{
		"simple": {in: FwRule{Name: "test", Action: "accept", Protocol: "tcp",
			DestAddr: "128.95.97.213/32", DestPort: "10130"},
			out: "test allow tcp 128.95.97.213/32 10130"},
		"negate-addr": {in: FwRule{Name: "test", DestAddr: "128.95.97.0/24", DestAddrNeg: true,
			DestPort: "80", Protocol: "tcp", Action: "accept"},
			out: "test allow tcp !128.95.97.0/24 80"},
		"negate-port": {in: FwRule{Name: "test", DestAddr: "128.95.97.0/24", DestPortNeg: true,
			DestPort: "80", Protocol: "tcp", Action: "accept"},
			out: "test allow tcp 128.95.97.0/24 !80"},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testFmt(t, tc.in, tc.out)
		})
	}
}

func TestTokenStore(t *testing.T) {
	tok := Token{
		Access:  "this_is_my_access_token",
		Refresh: "this_is_my_refresh_token",
		expires: time.Now().UTC().Add(10 * time.Minute),
	}

	var b bytes.Buffer
	if err := tok.Store(&b); err != nil {
		t.Fatal(err)
	}

	tok2 := Token{}
	tok2.Load(&b)

	if !tok2.IsValid() {
		t.Errorf("Loaded token invalid: %#v", tok2)
	}

	if !reflect.DeepEqual(tok, tok2) {
		t.Errorf("Expected %#v, got %#v", tok, tok2)
	}
}

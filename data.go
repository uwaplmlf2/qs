package qs

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

// Configuration type
type CfgType string

const (
	CfgRuntime CfgType = "runtime"
	CfgStartup         = "startup"
)

// Network protocols for firewall rules
type NetProtocol string

const (
	ProtoAny  NetProtocol = "any"
	ProtoTcp              = "tcp"
	ProtoUdp              = "udp"
	ProtoIcmp             = "icmp"
)

func (p *NetProtocol) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*p = NetProtocol(s)
	return nil
}

// Firewall rule actions
type FwAction string

const (
	ActionDrop   FwAction = "drop"
	ActionAccept          = "accept"
)

func (a FwAction) String() string {
	switch s := string(a); s {
	case "drop":
		return "deny"
	case "accept":
		return "allow"
	}
	return ""
}

func (a *FwAction) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	*a = FwAction(s)
	return nil
}

// FwRule represents an outgoing firewall rule
type FwRule struct {
	Name        string      `json:"name"`
	Protocol    NetProtocol `json:"protocol"`
	DestAddr    string      `json:"destAddr"`
	DestAddrNeg bool        `json:"destAddrNeg"`
	DestPort    string      `json:"destPort"`
	DestPortNeg bool        `json:"destPortNeg"`
	Action      FwAction    `json:"action"`
}

var ErrAction = errors.New("Invalid fw action")
var ErrProto = errors.New("Invalid fw protocol")

// UnmarshalText parses a firewall rule from a string
//
// name allow|deny protocol address port
func (fw *FwRule) ParseText(b []byte) error {
	text := string(bytes.ToLower(b))
	fields := strings.Split(text, " ")

	// Validity checks
	switch fields[1] {
	case "allow":
		fw.Action = ActionAccept
	case "deny":
		fw.Action = ActionDrop
	default:
		return fmt.Errorf("%q: %w", text, ErrAction)
	}

	switch fields[2] {
	case "any", "tcp", "udp", "icmp":
		fw.Protocol = NetProtocol(fields[2])
	default:
		return fmt.Errorf("%q: %w", text, ErrProto)
	}

	fw.Name = fields[0]
	if fields[3][0] == '!' {
		fw.DestAddr = fields[3][1:]
		fw.DestAddrNeg = true
	} else {
		fw.DestAddr = fields[3]
		fw.DestAddrNeg = false
	}

	if fields[4][0] == '!' {
		fw.DestPort = fields[4][1:]
		fw.DestPortNeg = true
	} else {
		fw.DestPort = fields[4]
		fw.DestPortNeg = false
	}

	return nil
}

func (fw FwRule) FmtText() ([]byte, error) {
	var b bytes.Buffer
	fmt.Fprintf(&b, "%s %s %s ", fw.Name, fw.Action, string(fw.Protocol))

	if fw.DestAddrNeg {
		fmt.Fprintf(&b, "!%s ", fw.DestAddr)
	} else {
		fmt.Fprintf(&b, "%s ", fw.DestAddr)
	}

	if fw.DestPortNeg {
		fmt.Fprintf(&b, "!%s", fw.DestPort)
	} else {
		fmt.Fprintf(&b, "%s", fw.DestPort)
	}

	return b.Bytes(), nil
}

func (fw FwRule) String() string {
	b, _ := fw.FmtText()
	return string(b)
}

// Device information
type DeviceInfo struct {
	Version string `json:"version"`
	Sn      string `json:"serial"`
	HwRev   string `json:"hwRevision"`
	ApiRev  string `json:"webApiVersion"`
	EthMac  string `json:"ethMac"`
	WifiMac string `json:"wifiMac"`
}

type TimeInfo struct {
	Sys int64 `json:"system"`
	Up  int64 `json:"uptime"`
}

// Status of the Iridium constellation link
type IrdConstellation struct {
	Visible  bool `json:"visible"`
	SigBars  int  `json:"signalBars"`
	SigLevel int  `json:"signalLevel"`
}

// Iridium hardware information
type IrdHw struct {
	Version string `json:"version"`
	Sn      string `json:"serial"`
	Imei    string `json:"imei"`
	AmpTemp int    `json:"paTemp"`
	BdTemp  int    `json:"boardTemp"`
}

// Iridium SIM card information
type IrdSim struct {
	Present   string `json:"present"`
	Connected bool   `json:"connected"`
	Iccid     string `json:"iccid"`
}
